---
title: To-do list
lang: zh-CN
date: 2023-01-04 15:21:27
---

1. 招贤纳士
2. 筹备新的论坛
3. 恢复项目的版本库
4. 在原有基于Debian主线版本的基础上推出基于Arch Linux的开发版
5. 解决构建问题
6. 开拓宣传渠道
